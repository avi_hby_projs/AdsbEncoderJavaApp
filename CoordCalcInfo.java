package com.avi.adsbenc;

public class CoordCalcInfo {

	private int modIndex;
	private double cprEven;
	private double cprOdd;
	private int cprEven17BitVal;
	private int cprOdd17BitVal;

	public CoordCalcInfo()
	{
		this.modIndex = 0;
		this.cprEven = 0.0;
		this.cprOdd = 0.0;
		this.cprEven17BitVal = 0;
		this.cprOdd17BitVal = 0;
	}

	public int getModIndex() {
		return modIndex;
	}

	public void setModIndex(int modIndex) {
		this.modIndex = modIndex;
	}

	public double getCprEven() {
		return cprEven;
	}

	public void setCprEven(double cprEven) {
		this.cprEven = cprEven;
	}

	public double getCprOdd() {
		return cprOdd;
	}

	public void setCprOdd(double cprOdd) {
		this.cprOdd = cprOdd;
	}

	public int getCprEven17BitVal() {
		return cprEven17BitVal;
	}

	public void setCprEven17BitVal(int cprEven17BitVal) {
		this.cprEven17BitVal = cprEven17BitVal;
	}

	public int getCprOdd17BitVal() {
		return cprOdd17BitVal;
	}

	public void setCprOdd17BitVal(int cprOdd17BitVal) {
		this.cprOdd17BitVal = cprOdd17BitVal;
	}

}
