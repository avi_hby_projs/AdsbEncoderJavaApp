package com.avi.adsbenc;

public class Util {

	private final static char[] HEX_LOOKUP = "0123456789ABCDEF".toCharArray();

	public static byte[] hexStrToByteArray(String data)
	{
		int length = data.length();
		byte[] bArray = new byte[length/2];

		for(int i = 0; i < length/2; ++i)
		{
			int base = i * 2;
			byte msNibble = (byte) Character.digit(data.charAt(base), 16);
			byte lsNibble = (byte) Character.digit(data.charAt(base + 1), 16);
			bArray[i] = (byte) ((msNibble << 4) | lsNibble);
		}

		return bArray;
	}

	public static String byteArrayToHexStr(byte[] data)
	{
		int length = data.length;
		char[] hexChar = new char[length * 2];

		for(int i = 0; i < length; ++i)
		{
			int base = i * 2;
			byte msNibble = (byte) ((data[i] >> 4) & 0x0F);
			byte lsNibble = (byte) (data[i] & 0x0F);
			hexChar[base] = HEX_LOOKUP[msNibble];
			hexChar[base + 1] = HEX_LOOKUP[lsNibble];
		}

		String hexStr = new String(hexChar);

		return hexStr;
	}

	public static double mod(double x, double y)
	{
		double mod = x % y;
		if(x < 0)
			mod += y;

		return mod;
	}

}
