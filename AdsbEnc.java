package com.avi.adsbenc;

public class AdsbEnc {

	public static final int ADSB_ODD_FRAME = 0;
	public static final int ADSB_EVEN_FRAME = 1;

    private static char[] ACID_LOOKUP = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ#####_###############0123456789######".toCharArray();
    private static int NZ = 15;
    private static double D_LAT_EVEN = 360.0 / (4 * NZ);
    private static double D_LAT_ODD = 360.0 / ((4 * NZ) - 1);

	public String[] encode(int typeCode, String icao24, double inpLat, double inpLong, int altitude)
	{
		if(typeCode < 9 || typeCode > 18)
			return null;

		String[] adsbData = new String[2];
		byte[] adsbDataOdd = new byte[14];
		byte[] adsbDataEven = new byte[14];

		setDownlinkHeader(adsbDataEven, adsbDataOdd);

		setIcao24(adsbDataEven, adsbDataOdd, icao24);

		setTypeCodeSSNIC(adsbDataEven, adsbDataOdd, typeCode, (byte) 0, (byte) 0);

		setAltitude(adsbDataEven, adsbDataOdd, altitude);

		// Position encoding

		// Latitude calculations
		CoordCalcInfo latitudeInfo = latitudeCalculations(inpLat);

		// Longitude calculations
		CoordCalcInfo longitudeInfo = longitudeCalculations(inpLat, inpLong, latitudeInfo.getCprEven17BitVal(), latitudeInfo.getCprOdd17BitVal());

		int latCprEven = latitudeInfo.getCprEven17BitVal();
		int longCprEven = longitudeInfo.getCprEven17BitVal();

		int latCprOdd = latitudeInfo.getCprOdd17BitVal();
		int longCprOdd = longitudeInfo.getCprOdd17BitVal();

		setLocation(adsbDataEven, adsbDataOdd, latCprEven, latCprOdd, longCprEven, longCprOdd);

		adsbData[ADSB_ODD_FRAME] = Util.byteArrayToHexStr(adsbDataOdd);
		adsbData[ADSB_EVEN_FRAME] = Util.byteArrayToHexStr(adsbDataEven);

		return adsbData;
	}

	private void setDownlinkHeader(byte[] adsbDataEven, byte[] adsbDataOdd)
	{
		adsbDataEven[0] = (byte) 0x8D;
		adsbDataOdd[0] = (byte) 0x8D;
	}

	private void setIcao24(byte[] adsbDataEven, byte[] adsbDataOdd, String icao24)
	{
		byte[] icao = Util.hexStrToByteArray(icao24);
		adsbDataEven[1] = icao[0];
		adsbDataEven[2] = icao[1];
		adsbDataEven[3] =  icao[2];

		adsbDataOdd[1] = icao[0];
		adsbDataOdd[2] = icao[1];
		adsbDataOdd[3] =  icao[2];
	}

	private void setTypeCodeSSNIC(byte[] adsbDataEven, byte[] adsbDataOdd, int typeCode, byte surveillanceStatus, byte nicsb)
	{
		adsbDataEven[4] = (byte) ((typeCode << 3) & 0xFF);
		adsbDataOdd[4] = (byte) ((typeCode << 3) & 0xFF);
	}

	private void setAltitude(byte[] adsbDataEven, byte[] adsbDataOdd, int altitude)
	{
		int n = (int) ((altitude + 1000)/25);
		int msPart = (n << 1) & 0xFE0;
		int lsPart = n & 0xF;

		// Set q-bit to 1
		msPart = msPart | 0x10;
		lsPart = (lsPart << 4) & 0xF0;

		adsbDataEven[5] = (byte) msPart;
		adsbDataEven[6] = (byte) lsPart;

		adsbDataOdd[5] = (byte) msPart;
		adsbDataOdd[6] = (byte) (lsPart | 0x04);
	}

	private void setLocation(byte[] adsbDataEven, byte[] adsbDataOdd, int latCprEven, int latCprOdd,
	        int longCprEven, int longCprOdd)
	{
		setLatitude(adsbDataEven, latCprEven);
		setLongitude(adsbDataEven, longCprEven);

		setLatitude(adsbDataOdd, latCprOdd);
		setLongitude(adsbDataOdd, longCprOdd);
	}

	private CoordCalcInfo latitudeCalculations(double inpLat)
	{
		CoordCalcInfo info = new CoordCalcInfo();

		int latCprEven17BitVal = (int) Math.floor((131072 * (Util.mod(inpLat, D_LAT_EVEN) / D_LAT_EVEN)) + 0.5);
		int latCprOdd17BitVal = (int) Math.floor((131072 * (Util.mod(inpLat, D_LAT_ODD) / D_LAT_ODD)) + 0.5);

		info.setCprEven17BitVal(latCprEven17BitVal);
		info.setCprOdd17BitVal(latCprOdd17BitVal);

		System.out.println("LatCprEven (17 bit value)= " + latCprEven17BitVal);
		System.out.println("LatCprOdd (17 bit value)= " + latCprOdd17BitVal);

		return info;
	}

	private CoordCalcInfo longitudeCalculations(double inpLat, double inpLong, int yzEven, int yzOdd)
	{
		CoordCalcInfo info = new CoordCalcInfo();

		double rLatEven = D_LAT_EVEN * (Math.floor(inpLat/D_LAT_EVEN) + yzEven/131072.0);
		double rLatOdd = D_LAT_ODD * (Math.floor(inpLat/D_LAT_ODD) + yzOdd/131072.0);

		double dLonEven = 360.0/Math.max(1.0, cprNL(rLatEven));
		double dLonOdd = 360.0/Math.max(1.0, (cprNL(rLatOdd) - 1));

		int longCprEven17BitVal = (int) Math.floor((131072 * (Util.mod(inpLong, dLonEven) / dLonEven)) + 0.5);
		int longCprOdd17BitVal = (int) Math.floor((131072 * (Util.mod(inpLong, dLonOdd) / dLonOdd)) + 0.5);

		info.setCprEven17BitVal(longCprEven17BitVal);
		info.setCprOdd17BitVal(longCprOdd17BitVal);

		System.out.println("LongCprEven (17 bit value)= " + longCprEven17BitVal);
		System.out.println("LongCprOdd (17 bit value)= " + longCprOdd17BitVal);

		return info;
	}

    private static int cprNL(double cprLat)
    {
    	int nl = 0;

        float a = (float) (1 - Math.cos(Math.PI / (2 * NZ)));
        float b = (float) Math.pow(Math.cos(Math.PI / 180.0 * Math.abs(cprLat)), 2);
        float nlF = (float) (2 * Math.PI / (Math.acos(1 - a/b)));
        nl = (int) Math.floor(nlF);

		return nl;
	}

	private void setLatitude(byte[] adsbData, int latCpr)
	{
		int latPart0 = (latCpr >> 15) & 0x03;
		int latPart1 = (latCpr >> 7) & 0xFF;
		int latPart2 = latCpr & 0x7F;

		adsbData[6] = (byte) (adsbData[6] | latPart0);
		adsbData[7] = (byte) (latPart1);
		adsbData[8] = (byte) (latPart2 << 1);
	}

	private void setLongitude(byte[] adsbData, int longCpr)
	{
		int longPart0 = (longCpr >> 16) & 0x01;
		int longPart1 = (longCpr >> 8) & 0xFF;
		int longPart2 = longCpr & 0xFF;

		adsbData[8] = (byte) (adsbData[8] | longPart0);
		adsbData[9] = (byte) (longPart1);
		adsbData[10] = (byte) (longPart2);
	}

	public static void main(String[] args) {

		//double inpLat = 42.122695;
		//double inpLong = -87.149291;
		double inpLat = 52.25720;
		double inpLong = 2.39148;
		int altitude = 38000;
		int typeCode = 11;
		String icao24 = "4840D6";

		AdsbEnc encoder = new AdsbEnc();

		String[] icao24Arr = new String[]{
				"AC82EC",
				"B0707A",
				"C3946B",
				"DB82B8",
				"40621D",
				"75804B"
		};
		int[] alt = new int[]{
				25672,
				31000,
				37596,
				19630,
				38000,
				2175
		};
		double[] inpCoords = new double[]{
				35.166456, -80.514633,
				34.140357, -77.629668,
				34.117613, -82.009319,
				36.095444, -83.047907,
				27.972450, -81.459414,
				31.186667, -84.416770
				};

		for(int i = 0; i < icao24Arr.length; ++i)
		{
			icao24 = icao24Arr[i];
			inpLat = inpCoords[i * 2];
			inpLong = inpCoords[(i * 2) + 1];
			altitude = alt[i];

			String[] adsbMessages = encoder.encode(typeCode, icao24, inpLat, inpLong, altitude);

			System.out.println("\nODD (T): " + adsbMessages[ADSB_ODD_FRAME]);
			System.out.println("EVEN (T + 1): " + adsbMessages[ADSB_EVEN_FRAME]);
		}


	}

}
